#!/bin/bash
# The following script runs Upstream CentOS functional tests with some tweaks for containerized context

yum install git jq patch -y

# Find the last commit that passed upstream's Jenkins
PASSING=$(curl -s https://ci.centos.org/job/CentOS-Core-QA-t_functional-c7-64/lastSuccessfulBuild/api/json | jq -r '.actions[] | select(.lastBuiltRevision) | .lastBuiltRevision.SHA1')
PASSING=${PASSING:-master}

git clone https://gitlab.cern.ch/linuxsupport/centos_functional_tests.git
cd centos_functional_tests
BRANCH=$(git reset --hard $PASSING)
echo -e "\e[1m\e[32m$BRANCH\e[0m"

# Disable certain tests from upstream
# We will add CERN specific tests on https://gitlab.cern.ch/linuxsupport/testing/cern_centos_functional_tests if needed
cat > ./skipped-tests.list  <<DELIM
# This file contains list of tests we need/want to skip
# Reason is when there is upstream bug that we're aware of
# So this file should contain:
#  - centos version (using $centos_ver)
#  - test to skip (tests/p_${name}/test.sh)
#  - reason why it's actually skipped (url to upstream BZ, or bug report)
# Separated by |
7|tests/0_common/00_centos_repos.sh|We want to keep CERN repos enabled
7|tests/0_common/20_upgrade_all.sh|Avoid too much noise on CI logs
7|tests/0_common/50_test_comps.sh|No need to test this here
7|tests/p_arpwatch/*|It is omitted in cc7 anyway
7|tests/p_audit/*|auditd not yet container ready
7|tests/p_autofs/*|No NFS on containers
7|tests/p_bind/*|Not possible in a container
7|tests/p_bridge-utils/*|Not possible in a container
7|tests/p_diffutils/*|For some reason cmp output changes between container and vm
7|tests/p_dovecot/*|Not relevant on containers
7|tests/p_freeradius/10_radiusd_test.sh|Depends on services, specifically removed initscripts on KS file
7|tests/p_httpd/*|No dbus
7|tests/p_initscripts/*|Depends on auditd, which is not yet container ready, also, specifically removed on KS
7|tests/p_ipa-server/*|No Freeipa default tests, also, we use cern-get-keytab
7|tests/p_kernel/*|No keyutils installed, also not relevant on containers
7|tests/p_libvirt/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_lftp/*|Could not yet make it run on privileged CI env
7|tests/p_lsof/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_lynx/*|No doc related tests as we use tsflags=nodocs yum config
7|tests/p_mailman/*|No dbus
7|tests/p_mod_python/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_mod_wsgi/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_mtr/mtr_test.sh|Fails too often
7|tests/p_mysql/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_net-snmp/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_network/*|Cannot create VLAN in containers
7|tests/p_nfs/*|NFS not relevant in a containerised context
7|tests/p_nmap/*|Not working on centos:7 either
7|tests/p_ntp/*|We have our own NTP servers, checking in a separate test
7|tests/p_openssh/*|Can't start service
7|tests/p_php*/*mariadb*|Can't start service
7|tests/p_php*/*mysql*|Can't start service
7|tests/p_postfix/*|Not relevant on containers
7|tests/p_postgresql/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_python*/*mysql*|Can't start service
7|tests/p_rsync/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_samba/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_selinux/*|It depends on auditd which is not yet container ready
7|tests/p_sendmail/*|No auditd therefore cannot be tested
7|tests/p_shim/*|No bootloader involved, boot path is always deleted so there is no secureboot
7|tests/p_spamassassin/spamassassin_test.sh|No doc related tests as we use tsflags=nodocs yum config
7|tests/p_squid/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_syslog/*|No rsyslog installed, also not relevant on containers
7|tests/p_systemd/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_telnet/10-test_telnet.sh|No systemd therefore cannot be tested
7|tests/p_tftp-server/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_tomcat/*|Depends on services, specifically removed initscripts on KS file
7|tests/p_vsftpd/*|No systemd therefore cannot be tested
7|tests/p_yum-plugin-fastestmirror/*|CERN CentOS does not have mirror list enabled
7|tests/r_check_mod_packages/*|Does not apply for CCentOS
7|tests/r_lamp/*|Depends on services, specifically removed initscripts on KS file
7|tests/z_rpminfo/*|Does not apply in our case
7|tests/z_repoclosure/*|Takes way too long
DELIM

# Don't try to read external resources, we may not have network connection
patch -p0 --ignore-whitespace <<'DELIM'
--- tests/p_curl/curl_test.sh    2020-07-21 19:33:04.136367594 +0200
+++ tests/p_curl/curl_test.sh    2020-07-21 19:34:24.078892598 +0200
@@ -6,8 +6,8 @@


 if [ $SKIP_QA_HARNESS -eq 1 ]; then
-  CHECK_FOR="The CentOS Project"
-  URL="http://www.centos.org/"
+  CHECK_FOR="CERN"
+  URL="http://linux.cern.ch/"
 else
   CHECK_FOR="Index of /srv"
   URL="http://repo.centos.qa/srv/CentOS/"
DELIM
# Exit if we couldn't patch the tests
[[ $? -eq 0 ]] || exit 1

find tests -type f -name "*.sh" -print0 | xargs -0 sed -i "s/ci.centos.org/linuxsoft.cern.ch/g"
find tests -type f -name "*.sh" -print0 | xargs -0 sed -i "s/mirror.centos.org/linuxsoft.cern.ch\/centos/g"

./runtests.sh
