# CERN CentOS 7 Docker images

http://cern.ch/linux/centos7/

## What is CERN CentOS 7 (CC7) ?

CERN Community ENTerprise Operating System 7 is the upstream CentOS 7, built to integrate into the CERN computing environment but it is not a site-specific product: all CERN site customizations are optional and can be deactivated for external users.

## Current release: CC7.9

CERN CentOS 7.9 is the current minor release of CC7.

### Image building

Refer to [koji-image-build](https://gitlab.cern.ch/linuxsupport/koji-image-build)
